#Stub makefile to install patches

PREFIX = $(DESTDIR)/usr
BINDIR = $(PREFIX)/bin
INSTALL = install
INSTALL_DIR = $(INSTALL) -d
INSTALL_FILE_BASE = $(INSTALL) -o root -g root
INSTALL_FILE = $(INSTALL_FILE_BASE) -m 644
INSTALL_EXE = $(INSTALL_FILE_BASE) -m 755

all:

install: powerpc-wrs-vxworks-munch powerpc-wrs-vxworks-stripsyms
	$(INSTALL_DIR) $(BINDIR)
	$(INSTALL_EXE) powerpc-wrs-vxworks-munch $(BINDIR)
	$(INSTALL_EXE) powerpc-wrs-vxworks-stripsyms $(BINDIR)
